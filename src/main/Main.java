package main;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;

import other.Account_Balance;
import other.BitSkinsListener;
import other.ConfigData;
import other.Serializer;

public class Main {


	public static int discount = 0;
	public static double miniPrice = 0;
	public static double maxPrice = 0;

	
	static HashMap<Integer, Boolean> hm = new HashMap<Integer, Boolean>();

	static Scanner scan = new Scanner(System.in);
	public static boolean verbose = true;
	public static boolean fullAuto = false;
	
	public static ConfigData config;
	public static Account_Balance balance;
	
	public static void main(String[] args) throws Exception {
		Main.scroll(32);


        
        
        try {
		config = (ConfigData) Serializer.deserialize("config.cfg");
		System.out.println("Welcome back!");
		}
		catch(FileNotFoundException e) {
			System.out.println("Welcome to the first time set up!");
			config = new ConfigData();
			config.API_KEY = getString("Enter your API key!");
			config.SECRET_KEY = getString("Enter your SECRET key!");
			Serializer.serialize("config.cfg", config);
			
		}

		try {
			Main.balance = Account_Balance.checkIp();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		discount = getInput("Select the minimum discount (%): ",0 , 100);
		miniPrice = getInputDouble("Select the minimum price ($): ",0 , 99999);
		maxPrice = getInputDouble("Select the maximum price ($): ", miniPrice , 99999999);
		verbose = getConfirmation("Set verbose(y/n)?");
		fullAuto = getConfirmation("Set full auto(y/n)?");
		
		
		
		
		
		PusherOptions options = new PusherOptions();
		options.setEncrypted(true);
		options.setHost("notifier.bitskins.com");
		options.setWsPort(443);
		options.setWssPort(443);
		Pusher pusher = new Pusher("c0eef4118084f8164bec65e6253bf195", options);
		pusher.subscribe("inventory_changes", new BitSkinsListener(), "listed");
		pusher.connect();
		

		boolean b = true;
        while(b){
            try {
            Thread.sleep(1);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
            }
        

		
		
		
	}
	
	public static String getString(String s) {
		System.out.print("\n"+s);
		String retstr = null;
		while(retstr == null) {
		try {
		String input = scan.next();
		retstr = input;
		}
		finally {
			if(retstr == null)
				System.out.println("Illegitimate input");
		}
		}
//		scan.close();
		return retstr;
	}
	
	public static int getInput(String s, int min, int max) {
		System.out.print("\n"+s);
		int retint = -1 ;
		while(retint < 0 || retint > max) {
		try {
		String input = scan.next();
		retint = Integer.parseInt(input);
		retint = (retint >= min && retint <= max) ? retint : -1;
		}
		catch(NumberFormatException e) {
		}
		finally {
			if(retint == -1)
				System.out.println("Illegitimate input");
		}
		}
//		scan.close();
		return retint;
	}

	public static double getInputDouble(String s, double min, double max) {
		System.out.print("\n"+s);
		double retDouble = -1 ;
		while(retDouble < 0 || retDouble > max) {
		try {
		String input = scan.next();
		retDouble = Double.parseDouble(input);
		retDouble = (retDouble >= min && retDouble <= max) ? retDouble : -1;
		}
		catch(NumberFormatException e) {
		}
		finally {
			if(retDouble == -1)
				System.out.println("Illegitimate input");
		}
		}
//		scan.close();
		return retDouble;
	}
	
	public static boolean getConfirmation(String s) {
		System.out.println(s);
		Boolean retbool = null;
		while(retbool == null) {
		try {
		String input = scan.next();
		if(input.equalsIgnoreCase("Y"))
			retbool = true;
				if(input.equalsIgnoreCase("N"))
					retbool = false;
		}
		catch(NumberFormatException e) {
		}
		finally {
			if(retbool == null)
				System.out.println("Illegitimate input");
		}
		}
		return retbool;
	}
	
	public static void scroll(int i) {
		for (int j = 0; j < i; j++) {
			System.out.println();
			
		}
	}
}
