package other;
import org.jboss.aerogear.security.otp.Totp;

public class Generator {
    public static String generatePassword(String secret) {
        return (new Totp(secret)).now();
    }
}