package other;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializer {
	/**
	 * Deserialize any object
	 * @param str
	 * @param cls
	 * @return
	 */
	public static Object deserialize(String fileName) throws IOException{
		FileInputStream fileInputStream;
		try {
			
			fileInputStream = new FileInputStream(new File(fileName));
		@SuppressWarnings("resource")
		ObjectInputStream stream=new ObjectInputStream(fileInputStream);
		
		return stream.readObject();
		
		}
//		catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return null;
	}
	/**
	 * Serialize any object
	 * @param obj
	 * @return
	 */
	public static Object serialize(String fileName, Object o) throws IOException{
	        File f = new File(fileName);
	        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
	        oos.writeObject(o);
	        oos.flush();
	        oos.close();
			return oos;
	    }
	
	
	
    public static void writeText(String filename, String text) {

      File currentFolder = new File(".");
      File workingFolder = new File(currentFolder, "itemIDs");
      if (!workingFolder.exists()) {
          workingFolder.mkdir();
      }
      
      
    	BufferedWriter writer = null;
//    	File test = new File(workingFolder+filename);
        try {
            //create a temporary file
//            String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
//            File logFile = new File(filename);

            // This will output the full path where the file will be written to...
//            System.out.println(workingFolder);
//            System.out.println(filename);
//            System.out.println(test.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(new File(workingFolder+"/"+filename)));
            writer.write(text);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }
	
	public static boolean file() {
		

//      File currentFolder = new File(".");
//      File workingFolder = new File(currentFolder, "runData");
//      if (!workingFolder.exists()) {
//          workingFolder.mkdir();
//      }
		return true;
	}
	
	
	
}
