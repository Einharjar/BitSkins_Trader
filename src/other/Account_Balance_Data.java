package other;
import com.google.gson.annotations.SerializedName;

public class Account_Balance_Data {
	@SerializedName("available_balance")
	double available_balance = 5;
	@SerializedName("pending_withdrawals")
	double pending_withdrawals = 5;
	@SerializedName("withdrawable_balance")
	double withdrawable_balance = 5;
	@SerializedName("couponable_balance")
	double couponable_balance = 5;
	

	public String toString() {
		return String.format("%s$ Available", available_balance, couponable_balance);
		
	}
//	public String toString() {
//		return String.format("%s$ Available, \n %s in coupons", available_balance, couponable_balance);
//	}
	
}
