package other;

import java.io.Serializable;

public class Listed_Data implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int app_id;
	int context_id;
	long item_id;
	long class_id;
	long instance_id;
	
	String image;
	String market_hash_name;
	
	double price;
	int discount;
	String event_type;
	double broadcasted_at;

	
	
	 public String toString() {
	
		 return market_hash_name +"\nPrice:"+ price +" (discount "+ discount +" %)";
	 }
	 
}
