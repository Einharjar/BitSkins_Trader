package other;

import java.util.Random;

public enum StringConstants {

	STRING_1("Welcome"),
	STRING_2("Välkommen"),
	STRING_3("tervetuloa"),
	STRING_4("желанный"),
	STRING_6("willkommen");

private final String text;

/**
 * @param text
 */
private StringConstants(String text) {
    this.text = text;
}

public static String getRandom() {

Random rand = new Random();

int  n = rand.nextInt(StringConstants.values().length-1) + 0;

return StringConstants.values()[n].text;
}

@Override
public String toString() {
    return text;
}
}
